Установить zabbix-agent2
----------------
``` sheell
Ubuntu 20.04:
   cd /tmp && wget https://repo.zabbix.com/zabbix/6.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_6.4-1+ubuntu20.04_all.deb && \
   dpkg -i zabbix-release_6.4-1+ubuntu20.04_all.deb && apt update && \
   apt install zabbix-agent2 zabbix-agent2-plugin-*

Debian 11 (Proxmox 7):
   cd /tmp && wget https://repo.zabbix.com/zabbix/6.4/debian/pool/main/z/zabbix-release/zabbix-release_6.4-1+debian11_all.deb
   dpkg -i zabbix-release_6.4-1+debian11_all.deb && apt update && \
   apt install zabbix-agent2 zabbix-agent2-plugin-*


sed -i s/Server\=127.0.0.1/Server\=82.148.26.176/ /etc/zabbix/zabbix_agent2.conf && \
systemctl enable zabbix-agent2 && \
systemctl restart zabbix-agent2
```


Proxmox настройка для мониторинга через zabbix-agent2
----------------
``` sheell
pveum user add zabbix@pam
pveum role add ZabbiMonitoring --privs "Sys.Audit VM.Audit"
pveum acl modify / -user zabbix@pam -role ZabbiMonitoring
```

Добовать токен в панели Proxmox
Datacenter -> Api Tokens -> add
```
user: zabbix@pam
Token ID: zabbix.monitoring
uncheck: Privilege Separation
```
-> add

Скопирвоать Token ID и Secret в секцию макросов 

