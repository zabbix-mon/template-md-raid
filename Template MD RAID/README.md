Zabbix template handles Software RAID (MD) on Linux
==================

Добавить в /etc/sudoers файл
----------------
``` sheell
   zabbix ALL= (ALL) NOPASSWD: /usr/local/bin/zabbix_mdraid.sh
```

Добавить в zabbix_agentd.conf файл
----------------
``` sheell
   UserParameter=mdraid[*], sudo /usr/local/bin/zabbix_mdraid.sh -m'$1' -$2'$3'
   UserParameter=mdraid.discovery, sudo /usr/local/bin/zabbix_mdraid.sh -D 
```

Скопирвоать и изменить права
----------------
``` sheell
   cp zabbix_mdraid.sh /usr/local/bin
   chmod +x /usr/local/bin/zabbix_mdraid.sh
   chown zabbix:zabbix /usr/local/bin/zabbix_mdraid.sh 
```

Перезапустить агент
----------------
``` sheell
   systemctl restart zabbix-agent2 
```
